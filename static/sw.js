importScripts('/_nuxt/workbox.6041bd81.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/01f2d90b12f193a42a86.js",
    "revision": "30326b4517ef68ad66cbfe9af19ebb33"
  },
  {
    "url": "/_nuxt/0f32db01098be7e67ec1.js",
    "revision": "3fd553ff08eb2c078d390980d6b22893"
  },
  {
    "url": "/_nuxt/14ac904159b83abaa502.js",
    "revision": "90b9b894177efbba3086d83223f36157"
  },
  {
    "url": "/_nuxt/1c1e04a2c242892e1d19.js",
    "revision": "df1938ace97a69c8133b14df6f6c7961"
  },
  {
    "url": "/_nuxt/211a2a18e3130b7e0ee6.js",
    "revision": "91dd00e340e76d0e44dbaa34f716edc5"
  },
  {
    "url": "/_nuxt/256620813e310465ffcd.js",
    "revision": "fb23181029a9df0adbfc5eca2e9bb16b"
  },
  {
    "url": "/_nuxt/2ff355c9453dd0352217.js",
    "revision": "6f77938d0586739ff4284997bc09dc63"
  },
  {
    "url": "/_nuxt/383910566d32652f2bc8.js",
    "revision": "c5c2a0e267f1590965e759c07a4e00b9"
  },
  {
    "url": "/_nuxt/5493796d5f6ba0e24f22.js",
    "revision": "d394275760e619531c5484fe8c676bdc"
  },
  {
    "url": "/_nuxt/6012c0cc52e780ce3127.js",
    "revision": "b4ee575a3dc8d20e6010127220b5b20e"
  },
  {
    "url": "/_nuxt/619a270079621f7ef597.js",
    "revision": "b91834d81d140bce05adffb6848d86be"
  },
  {
    "url": "/_nuxt/61dbd1bb96576fa0ed2b.js",
    "revision": "baf6c3bb131cea4b554ff30bc701893d"
  },
  {
    "url": "/_nuxt/7597c73c4c7ad1536b63.js",
    "revision": "fd186567a09087cb76d3d7b7f2c8d8ae"
  },
  {
    "url": "/_nuxt/7602a670e8723981031a.js",
    "revision": "51247ed9a56f21eda6faa536b82b2333"
  },
  {
    "url": "/_nuxt/77d9531f289549bc4459.js",
    "revision": "24d1ad35b45ab90e5161fc4d1d2df7f0"
  },
  {
    "url": "/_nuxt/7de425e43d86d083206d.js",
    "revision": "75598943d3fff4529f3b2e9c56403dff"
  },
  {
    "url": "/_nuxt/7e60d53724a2983be370.js",
    "revision": "f5d305814f707d02c0b0431e3e62da74"
  },
  {
    "url": "/_nuxt/8a8fb89a20ca67e76f80.js",
    "revision": "99de2e5a4ac80603417aa47d1db8953d"
  },
  {
    "url": "/_nuxt/981e35fe1f4fad6bce73.js",
    "revision": "0fbc20d64fd6f1baa50e49546f29a54f"
  },
  {
    "url": "/_nuxt/af71314f5916a64a54ff.js",
    "revision": "a3a98f22351f2591f411b98082cbe5d0"
  },
  {
    "url": "/_nuxt/b4f83bb45c9a33ac2a40.js",
    "revision": "b2d62a39307138e7ee6cabeef5bc0fb3"
  },
  {
    "url": "/_nuxt/baa4f28800f643d2f8a6.js",
    "revision": "a256a7c5af200a3360bf651efb5f852f"
  },
  {
    "url": "/_nuxt/c355366d19b7fab3da0e.js",
    "revision": "bc97d4eafa8b349514c332f5e609ecbc"
  },
  {
    "url": "/_nuxt/c3b8cc5bceada27b8f1f.js",
    "revision": "64b650d038011c07e1ac3867fe76c88d"
  },
  {
    "url": "/_nuxt/d352a212aecaea51fdb7.js",
    "revision": "4b607c112c888a8043468b852047871c"
  },
  {
    "url": "/_nuxt/dbfe9cda95cbd15dba62.js",
    "revision": "bda764e690eb917d539fead7da28e3cb"
  },
  {
    "url": "/_nuxt/e143ce4c348dfbd9d969.js",
    "revision": "1d847431a12b4db3c0cd77d462f4f61c"
  },
  {
    "url": "/_nuxt/e77c78fd2b6d23947167.js",
    "revision": "83fe680a723697c9bcb296370287f5e5"
  },
  {
    "url": "/_nuxt/ef504241a36404414fca.js",
    "revision": "d5a3497aadff30fe058a87d9dadda72f"
  },
  {
    "url": "/_nuxt/f0ca96b656d4c81a2e01.js",
    "revision": "d9ca6be27530386cd856920fe3472479"
  },
  {
    "url": "/_nuxt/f2556c91e25ccc08a0e7.js",
    "revision": "8d11cc8c8f84acd968936779c4136a00"
  },
  {
    "url": "/_nuxt/fa341654df8d3cd8833e.js",
    "revision": "af2d34bcfa37a0a9f5305177259a46e3"
  }
], {
  "cacheId": "zaSpasibo",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
